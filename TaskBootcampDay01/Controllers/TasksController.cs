﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskBootcampDay01.Logics;
using TaskBootcampDay01.Models;

namespace TaskBootcampDay01.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        [HttpGet]
        [Route("GetUserWithTask")]
        public ActionResult GetUserWithTask([FromQuery] string? name = null)
        {
            try
            {
                List<object> result = new List<object>();
                result = TasksLogics.GetUserWithTask(name);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("AddUserWithTask")]
        public ActionResult AddUserWithTask([FromBody] TasksModel userTask)
        {
            try
            {
                List<object> result = new List<object>();
                TasksLogics.AddUserWithTask(userTask);

                return Ok("created");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
