﻿using System.Data.SqlClient;
using TaskBootcampDay01.Models;

namespace TaskBootcampDay01.Logics
{
    public class TasksLogics
    {
        private static string connectionString = "Server=Iskandar;Database=TaskApiSql;Trusted_Connection=True;";

        public static List<object> GetUserWithTask(string? name = null)
        {
            List<object> result = new List<object>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    if (string.IsNullOrEmpty(name))
                    {
                        cmd.CommandText = "SELECT * FROM users INNER JOIN tasks ON users.pk_users_id = tasks.fk_users_id ORDER BY tasks.pk_tasks_id;";
                    }
                    else
                    {
                        cmd.CommandText = "SELECT * FROM users INNER JOIN tasks ON users.pk_users_id = tasks.fk_users_id WHERE name=@name ORDER BY tasks.pk_tasks_id;";
                        cmd.Parameters.AddWithValue("@name", name);
                    }

                    SqlDataReader reader = cmd.ExecuteReader();

                    int lastUserId = -1;
                    object? currentUser = null;
                    List<object>? currentTask = null;

                    while (reader.Read())
                    {
                        int userId = (int)reader["pk_users_id"];
                        if (userId != lastUserId)
                        {
                            if (currentUser != null)
                            {
                                result.Add(currentUser);
                            }

                            currentTask = new List<object>();
                            currentUser = new
                            {
                                tasks = currentTask,
                                pk_users_id = userId,
                                name = reader["name"]
                            };

                            lastUserId = userId;
                        }

                        currentTask.Add(new
                        {
                            pk_tasks_id = reader["pk_tasks_id"],
                            task_detail = reader["task_detail"]
                        });
                    }
                    reader.Close();


                    if (currentUser != null)
                    {
                        result.Add(currentUser);
                    }

                }

                conn.Close();
            }

            return result;
        }

        public static void AddUserWithTask(TasksModel userTask)
        {
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using(SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO users (name) VALUES (@name); SELECT CAST(SCOPE_IDENTITY() AS INT)";
                    cmd.Parameters.AddWithValue("@name", userTask.name);

                    int userId = (int)cmd.ExecuteScalar();

                    for (int i = 0; i < userTask.TaskDetail.Count; i++)
                    {
                        Tasks task = userTask.TaskDetail[i];
                        cmd.CommandText = "INSERT INTO tasks (fk_users_id, task_detail) VALUES (@fk_users_id, @task_detail)";
                        cmd.Parameters.AddWithValue("@fk_users_id", userId);
                        cmd.Parameters.AddWithValue("@task_detail", task.task_detail);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                conn.Close();
            }
            
        }

    }
}
